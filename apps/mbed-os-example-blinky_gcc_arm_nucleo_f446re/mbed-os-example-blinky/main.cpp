#include "mbed.h"
//#include <string>
//using namespace std;
//a#include "TextLCD.h"
 
 
#define READBUFFERSIZE (32)
 
Serial g_serial(PA_9, PA_10);
Serial out(USBTX,USBRX);
DigitalOut myled(LED1);
 
int ReadLineString( Serial& serial,
                    char szReadBuffer[], const int ciReadBufferSize, int& riIndexChar,
                    char szLineString[], const int ciLineStringSize )
{
    while( 1 )
    {
        if( !serial.readable() )
        {
            break;
        }
        char c = serial.getc();
        if( '\r' == c  )
        {
            szReadBuffer[riIndexChar] = '\0';
            strncpy( szLineString, szReadBuffer, ciLineStringSize - 1 );
            szLineString[ciLineStringSize - 1] = '\0';
            riIndexChar = 0;
            return 1;
        }
        else if( '\n' == c )
        {
            ;
        }
        else
        {
            if( (ciReadBufferSize - 1) > riIndexChar )
            {
                szReadBuffer[riIndexChar] = c;
                riIndexChar++;
            }
        }
    }
 
    return 0;
}


int Menu()
{
	
	 // loop
    //int iCounter = 0;
	//string cara;
	//char szLineString;
    char szReadBuffer[READBUFFERSIZE] = "";
    int iIndexChar = 0;
	
	out.printf("This is the Menu\n\r");
	out.printf("a) read\n\r");
	out.printf("b) write\n\r");
	out.printf("c) Led ON/OFF\n\r");
			
	while(1)
    {
		char szLineString[READBUFFERSIZE];
		if( !ReadLineString( out, 
                             szReadBuffer, READBUFFERSIZE, iIndexChar,
                             szLineString, READBUFFERSIZE ) )
        {
            continue;
        }
		//cara = szLineString;
		if(szLineString[0]=='a')
		{
			out.printf("READ\n\r");
		}
		if(szLineString[0]=='b')
		{
			out.printf("WRITE\n\r");
		}
		if(szLineString[0]=='c')
		{
			myled = !myled;
			out.printf("Led\n\r");
		}
		
	}
}	

 
int main()
{

    g_serial.baud(9600);
    wait(0.001);
 
    // loop
    //int iCounter = 0;
    char szReadBuffer[READBUFFERSIZE] = "";
    int iIndexChar = 0;
    while(1)
    {
  
		
        char szLineString[READBUFFERSIZE];
        if( !ReadLineString( out, 
                             szReadBuffer, READBUFFERSIZE, iIndexChar,
                             szLineString, READBUFFERSIZE ) )
        {
            continue;
        }
		
		//selezione menu
		if( szLineString[0] == 'm' )
        {
            Menu();
        }

		
        out.printf( "Ecco la stringa: %s\n", szLineString );
    }
}